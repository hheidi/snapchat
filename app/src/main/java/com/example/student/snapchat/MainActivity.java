package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuWrapperFactory;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID ="9EFA35BC-4F19-B4A7-FF8A-D450F8286B00";
    public static final String SECRET_KEY ="B7A41890-8AC8-7C0F-FF64-A650D23CA500";
    public static final String VERSION ="v1";

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if(Backendless.UserService.loggedInUser() == ""){
            MainMenuFragment mainMenuFragment = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenuFragment).commit();
        }else{
            LoginMenu loginMenu = new LoginMenu();
            getSupportFragmentManager().beginTransaction().add(R.id.container, loginMenu).commit();


        }
    }
}
