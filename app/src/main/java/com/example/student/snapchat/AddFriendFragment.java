package com.example.student.snapchat;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.DialogPreference;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class AddFriendFragment extends Fragment {


    public AddFriendFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_friend, container, false);

        Button addFriendButton = (Button) view.findViewById(R.id.addFriendButton);
        addFriendButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alerDialog = new AlertDialog.Builder(getActivity());
                alerDialog.setMessage("Add a Friend.");

                EditText inputField = new EditText(getActivity());
                alerDialog.setView(inputField);

                alerDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                alerDialog.setPositiveButton("Add Friend", new DialogInterface.OnClickListener() {)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        addFriend(inputField.getText().toString());

                    }
                });
            }

        });

        return view;
//        return inflater.inflate(R.layout.fragment_add_friend, container, false);
    }
    private void addFriend(String friendName){

}
