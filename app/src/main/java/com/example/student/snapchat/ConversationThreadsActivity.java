package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class ConversationThreadsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ConversationThreads conversationThreads = new ConversationThreads();
        getSupportFragmentManager().beginTransaction().add(R.id.container, conversationThreads).commit();
    }
}
