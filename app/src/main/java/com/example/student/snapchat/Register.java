package com.example.student.snapchat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Register extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RegisterFragment registerFragment = new RegisterFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, registerFragment).commit();
    }
}